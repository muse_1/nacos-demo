package com.muse.nacos.provider.sdk;

/**
 * @description
 * @author: muse
 **/
public interface OrderService {

    String getOrder();
}
