package com.muse.nacos.simple;

import java.util.concurrent.Executor;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.Listener;

public class NacosDemoListener {
    private final static String SERVER_ADDR = "127.0.0.1:8848";
    private final static String DATE_ID_1 = "nacos.cfg.dataId";
    private final static String GROUP_1 = "test";

    public static void main(String[] args) throws Throwable {
        ConfigService configService = NacosFactory.createConfigService(SERVER_ADDR);
        configService.addListener(DATE_ID_1, GROUP_1, new Listener() {
            public Executor getExecutor() {
                return null;
            }
            // 修改namespace="public", data_id="nacos.cfg.dataId", group="test"的配置项之后，该方法会被调用
            public void receiveConfigInfo(String configInfo) {
                System.out.println("addListener = " +  configInfo);
            }
        });
        System.in.read();
    }
}
