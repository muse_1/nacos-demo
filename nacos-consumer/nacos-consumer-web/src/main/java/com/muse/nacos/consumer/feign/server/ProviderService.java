package com.muse.nacos.consumer.feign.server;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @description
 * @author: muse
 **/
@FeignClient(name = "nacos-provider")
public interface ProviderService {

    @GetMapping("/provider/hello")
    String hello();
}
