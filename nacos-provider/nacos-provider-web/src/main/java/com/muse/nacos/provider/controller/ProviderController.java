package com.muse.nacos.provider.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequestMapping("/provider")
@RestController
public class ProviderController {
    /**
     * http://localhost:7001/provider/hello
     */
    @RequestMapping("/hello")
    public String hello() {
        log.info("provider hello");
        return "provider hello";
    }
}
