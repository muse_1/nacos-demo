package com.muse.nacos.consumer.controller;

import javax.annotation.Resource;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.muse.nacos.consumer.feign.server.ProviderService;
import com.muse.nacos.provider.sdk.OrderService;

import lombok.extern.slf4j.Slf4j;

/**
 * @description
 * @author: muse
 **/
@Slf4j
@RequestMapping("/consumer")
@RestController
public class ConsumerController {

    @Reference // org.apache.dubbo.config.annotation
    private OrderService orderService;

    @Resource
    private ProviderService providerService;

    /**
     * http://localhost:7002/consumer/hello
     */
    @RequestMapping("/hello")
    public String hello() {
        log.info("OpenFeign invoke!");
        return providerService.hello();
    }

    /**
     * http://localhost:7002/consumer/getOrder
     */
    @RequestMapping("/getOrder")
    public String getOrder() {
        log.info("Dubbo invoke!");
        return orderService.getOrder();
    }
}
