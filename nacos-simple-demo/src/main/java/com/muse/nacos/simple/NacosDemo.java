package com.muse.nacos.simple;

import java.util.Properties;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.PropertyKeyConst;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;

/**
 * @description
 * @author: muse
 **/
public class NacosDemo {
    private final static String SERVER_ADDR = "127.0.0.1:8848";
    private final static String DATE_ID_1 = "nacos_demo_1";
    private final static String DEFAULT_GROUP = "DEFAULT_GROUP";
    private final static Long TIMEOUT_MS = 1000L;
    private final static String MUSE_NAMESPACE = "93b8b053-0286-4824-82b1-00a9226549dc";

    public static void main(String[] args) {
        try {
            /** 1：查询默认命名空间（public）的配置信息 */
            ConfigService configService = NacosFactory.createConfigService(SERVER_ADDR);
            String configContent1 = configService.getConfig(DATE_ID_1, DEFAULT_GROUP, TIMEOUT_MS);
            System.out.println("configContent1 = " +  configContent1);

            /** 2：查询muse命名空间的配置信息 */
            Properties properties = new Properties();
            properties.put(PropertyKeyConst.SERVER_ADDR, SERVER_ADDR);
            properties.put(PropertyKeyConst.NAMESPACE, MUSE_NAMESPACE); // 指定命名空间为muse，切记：此处使用NameSpace ID
            configService = NacosFactory.createConfigService(properties);
            String configContent2 = configService.getConfig(DATE_ID_1, DEFAULT_GROUP, TIMEOUT_MS);
            System.out.println("configContent2 = " +  configContent2);
        } catch (NacosException e) {
            e.printStackTrace();
            System.out.println("error Msg=" + e.getErrMsg());
        }
    }
}
