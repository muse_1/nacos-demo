package com.muse.nacos.simple;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @description
 * @author: muse
 **/
public class PasswordGenericUtils {
    public static void main(String[] args) {
        System.out.println(new BCryptPasswordEncoder().encode("bob"));
    }
}
