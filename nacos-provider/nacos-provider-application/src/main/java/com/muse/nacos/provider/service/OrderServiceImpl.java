package com.muse.nacos.provider.service;

import org.apache.dubbo.config.annotation.Service;

import com.muse.nacos.provider.sdk.OrderService;

import lombok.extern.slf4j.Slf4j;

/**
 * @description
 * @author: muse
 **/
@Slf4j
@Service // org.apache.dubbo.config.annotation
public class OrderServiceImpl implements OrderService {

    public String getOrder() {
        log.info("Order Info");
        return "Order Info";
    }
}
