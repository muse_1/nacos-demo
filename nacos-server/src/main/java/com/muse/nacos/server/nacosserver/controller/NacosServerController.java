package com.muse.nacos.server.nacosserver.controller;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description
 * @author: muse
 **/
// @RefreshScope
@RestController
public class NacosServerController {

    @Resource
    private ConfigurableApplicationContext configurableApplicationContext;

    @Value("${student.name:null}")
    private String name;

    @Value("${config2.name:null}")
    private String config2ame;

    @Value("${config3.name:null}")
    private String config3ame;

    @Value("${config4.name:null}")
    private String config4ame;

    /**
     * http://localhost:9002/name
     * 如果只是通过@Value获得的配置信息，不会随着Nacos的修改操作而获得最新配置信息。那么，实时获取最新配置信息有两种方式：
     * 方式1：添加@RefreshScope注解。
     * 方式2：通过getProperty的方式，获得最新的配置信息。
     *
     * @return
     */
    @GetMapping("/name")
    public String getName() {
        String name1 = configurableApplicationContext.getEnvironment().getProperty("student.name");
        return String.format("name=%s <br> name1=%s", name, name1);
    }

    /**
     * http://localhost:9002/allname
     */
    @GetMapping("/allname")
    public String getAllName() {
        return String.format("config1Name=%s<br> config2Name=%s<br> config3Name=%s<br> config4Name=%s", name,
                config2ame, config3ame, config4ame);
    }
}
